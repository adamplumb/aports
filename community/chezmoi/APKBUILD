# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=chezmoi
pkgver=2.5.1
pkgrel=0
pkgdesc="Manage your dotfiles across multiple machines, securely."
url="https://www.chezmoi.io/"
arch="all"
license="MIT"
makedepends="go"
options="!check chmod-clean" # no test suite
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/twpayne/chezmoi/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir"

build() {
	go build \
		-ldflags "
		-X \"main.version=$pkgver\" \
		-X \"main.date=$(date --utc +%Y-%m-%dT%H:%M:%SZ)\" \
		-X \"github.com/twpayne/chezmoi/cmd.DocsDir=/usr/share/doc/chezmoi/\"
		" \
		-tags noupgrade \
		-tags noembeddocs
	make completions
}

package() {
	install -Dm0755 chezmoi "$pkgdir"/usr/bin/chezmoi
	install -Dm0644 completions/chezmoi-completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm0644 completions/chezmoi.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
	install -Dm0644 completions/chezmoi.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc/chezmoi"
	cp "$builddir/docs/"* "$subpkgdir/usr/share/doc/chezmoi"
}

sha512sums="
d27e20dbb12198d3fd376c0eef7402594dbf7f2adb20edd04d954ba26688cb3a3fec7fc4f2180cc65f4bb58574bd5c1796efa19a68139b41cd5a23635056f70c  chezmoi-2.5.1.tar.gz
"
